#!/bin/bash

cd .git/hooks
for f in ../../git-hooks/*; do
  ln -s $f $(basename $f);
done
cd ../..
